/**
 *
 *
 *  @type {Object}
 *  @name Glance
 */
class Glance {
  constructor(client) {
    this.client = client;
  }

  /**
   * @deprecated use updateGlanceState method from Modules class instead
   * @description  updates glance state.
	 * @name  Glance.Update Glance
	 * @route {POST} /app/module/chat/conversation/chat:glance/${glanceKey}/state
	 * @param {string} glanceKey Glance Key used to update stride. See Example.
	 * @param {string} opts body = {  context: {  cloudId, conversationId, userId } }
	 * @return {empty} none
	 * @link https://developer.atlassian.com/cloud/stride/rest/#api-app-module-chat-conversation-chat-glance-key-state-post
	 * @example Descriptor.json
	 *
	 "chat:glance": [
	 {
	   "key": "glance-showcase",
	   "name": {
		 "value": "App Glance"
	   },
	   "icon": {
		 "url": "/public/img/logo.png",
		 "url@2x": "/public/img/logo.png"
	   },
	   "target": "actionTarget-openSidebar-showcase",
	   "queryUrl": "/module/glance/state",
	   "authentication": "jwt"
	 }
	 ]
	 */
  async updateState(glanceKey, opts) {
    const endpoint = `/app/module/chat/conversation/chat:glance/${glanceKey}/state`;
    return this.client.post(endpoint, opts);
  }
}

module.exports = function(client) {
  return new Glance(client);
};
